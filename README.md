<!--
SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>

SPDX-License-Identifier: CC0-1.0
-->

# elixir-eris

Elixir implementation of Encoding for Robust Immutable Storage ([ERIS](http://purl.org/eris)).

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `eris` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:eris, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/eris](https://hexdocs.pm/eris).

## Usage

``` elixir
> ERIS.encode_urn("Hail ERIS!")
"urn:erisx2:AAAAV4OIFHWY67XFEHAOQVXUOWTYDVG5TEY6S6IW4PJ4SQLVJJF4MIKNDLKUDPPHDCKLBUIAJQ3U2IEARRPFHEHWFW5NJY7BJUGFESPGDQ"

> {read_capability, block_storage} = ERIS.encode("Hail ERIS!", %{})
{%ERIS.ReadCapability{
   block_size: 1024,
   key: <<77, 26, 213, 65, 189, 231, 24, 148, 176, 209, 0, 76, 55, 77, 32, 128,
     140, 94, 83, 144, 246, 45, 186, 212, 227, 225, 77, 12, 82, 73, 230, 28>>,
   level: 0,
   reference: <<10, 241, 200, 41, 237, 143, 126, 229, 33, 192, 232, 86, 244,
     117, 167, 129, 212, 221, 153, 49, 233, 121, 22, 227, 211, 201, 65, 117, 74,
     75, 198, 33>>
 },
 %{
   <<10, 241, 200, 41, 237, 143, 126, 229, 33, 192, 232, 86, 244, 117, 167, 129,
     212, 221, 153, 49, 233, 121, 22, 227, 211, 201, 65, 117, 74, 75, 198,
     33>> => <<199, 82, 167, 68, 44, 144, 25, 137, 73, 182, 232, 146, 221, 153,
     237, 102, 60, 111, 49, 59, 37, 154, 64, 147, 182, 210, 167, 231, 205, 187,
     185, 39, 131, 67, 205, 231, 242, 175, 182, 68, 213, 97, 151, 159, 241, 131,
     139, ...>>
 }}
 
> ERIS.decode(read_capability, block_storage)
"Hail ERIS!"
```
