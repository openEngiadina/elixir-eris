# SPDX-FileCopyrightText: 2020-2021 pukkamustard <pukkamustard@posteo.net>
# SPDX-FileCopyrightText: 2020-2021 rustra <rustra@disroot.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

defmodule ERIS.Crypto do
  @moduledoc """
  Provides the cryptographic primitives required for the ERIS encoding (see
  http://purl.org/eris#_cryptographic_primitives).

  Uses the [Monocypher](https://monocypher.org/) cryptographic library via
  [erlang-monocypher](https://gitlab.com/openengiadina/erlang-monocypher).
  """

  @doc """
  Returns the ChaCha20 cipher text of `data` given `key` (using null nonce).
  """
  @spec chacha20(binary, keyword) :: binary
  def chacha20(data, key: key) do
    :monocypher.crypto_ietf_chacha20(data, key, <<0::96>>)
  end

  @doc """
  Returns the Blake2b-256 hash of `data`.
  """
  @spec blake2b(binary) :: binary
  def blake2b(data) do
    :monocypher.crypto_blake2b_general(32, <<>>, data)
  end

  @doc """
  Returns the Blake2b-256 hash of `data` using the hashing key `key`.
  """
  @spec blake2b(binary, keyword) :: binary
  def blake2b(data, key: key) do
    :monocypher.crypto_blake2b_general(32, key, data)
  end

  @doc """
  Returns `data` padded to a multiple of `block_size`.
  """
  @spec pad(binary, keyword) :: binary
  def pad(data, block_size: block_size) do
    data_size = byte_size(data)
    pad_len = ((div(data_size, block_size) + 1) * block_size - data_size - 1) * 8

    data <> <<0x80::8>> <> <<0::size(pad_len)>>
  end

  @doc """
  Returns unpadded `data`.
  """
  @spec unpad(binary, keyword) :: {:ok, binary} | {:error, any}
  def unpad(data, block_size: block_size) do
    data_size = byte_size(data)

    if data_size < block_size or rem(data_size, block_size) != 0 or block_size <= 0 do
      {:error, :invalid_padding}
    else
      unpad_loop(data)
    end
  end

  @spec unpad_loop(binary) :: {:ok, binary} | {:error, atom}
  defp unpad_loop(data) do
    data_size = byte_size(data)

    case binary_part(data, data_size, -1) do
      <<0x00>> -> unpad_loop(binary_part(data, 0, data_size - 1))
      <<0x80>> -> {:ok, binary_part(data, 0, data_size - 1)}
      _ -> {:error, :invalid_padding}
    end
  end
end
