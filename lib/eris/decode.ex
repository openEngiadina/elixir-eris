# SPDX-FileCopyrightText: 2020-2021 pukkamustard <pukkamustard@posteo.net>
# SPDX-FileCopyrightText: 2020-2021 rustra <rustra@disroot.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

defmodule ERIS.Decode do
  @moduledoc """
  ERIS stream decoder. Returns a stream that emits decoded blocks.
  """

  alias ERIS.{BlockStorage, Crypto, ReadCapability}

  defmodule Node do
    @moduledoc """
    Implements a Zipper (see [Gerard Huet - The
    Zipper](https://www.st.cs.uni-saarland.de//edu/seminare/2005/advanced-fp/docs/huet-zipper.pdf))
    that is used to traverse the nodes of the ERIS Merkle Tree.

    Unlike the original Zipper siblings are not trees but reference-key pairs
    that are decoded and resolved lazily. When implementing random-access it
    will make sense to cache trees as siblings.

    Nodes represent nodes in the ERIS Merkle Tree and not reference-key pairs.
    This is why the corresponding level for a reference-key pair is one higher.
    """

    defstruct level: 1, left: [], up: :top, right: [], last?: false

    @type t :: %__MODULE__{
            level: number,
            left: [{binary, binary}],
            up: :top | t,
            right: [{binary, binary}],
            last?: bool
          }

    @doc """
    Create a node from a read capability.
    """
    @spec from_read_capability(ReadCapability.t()) :: t
    def from_read_capability(%ReadCapability{} = read_capability) do
      %__MODULE__{
        level: read_capability.level + 1,
        left: [],
        up: :top,
        right: [{read_capability.reference, read_capability.key}],
        last?: true
      }
    end

    defp chunk_binary(data, block_size) do
      Stream.resource(
        fn -> data end,
        fn data ->
          case data do
            <<>> ->
              {:halt, <<>>}

            <<head::binary-size(block_size), rest::binary>> ->
              {[head], rest}

            _ ->
              {[data], <<>>}
          end
        end,
        fn _ -> :ok end
      )
    end

    defp get_block(block_storage, reference) do
      with {:ok, block} <- BlockStorage.get(block_storage, reference) do
        if Crypto.blake2b(block) == reference do
          {:ok, block}
        else
          {:error, :corrupt_block}
        end
      end
    end

    defp decode_block(block, key) do
      block
      |> Crypto.chacha20(key: key)
      |> chunk_binary(64)
      |> Stream.transform(
        nil,
        fn rk_pair, _ ->
          case rk_pair do
            <<0::512>> ->
              {:halt, nil}

            <<reference::binary-size(32)>> <> <<key::binary-size(32)>> ->
              {[{reference, key}], nil}
          end
        end
      )
      |> Enum.to_list()
    end

    @doc """
    Get block from BlockStorage and traverse the tree down.
    """
    @spec down(t, BlockStorage.t()) :: t
    def down(%__MODULE__{level: level, right: [{reference, key} | tail]} = node, block_storage)
        when level > 1 do
      with {:ok, block} <- get_block(block_storage, reference) do
        %__MODULE__{
          level: level - 1,
          left: [],
          up: node,
          right: decode_block(block, key),
          last?: Enum.empty?(tail) and node.last?
        }
      end
    end

    @doc """
    Attempt to go right
    """
    @spec right(t) :: t
    def right(%__MODULE__{right: []} = node), do: node

    def right(%__MODULE__{left: left, right: [head | tail]} = node) do
      %{node | left: left ++ [head], right: tail}
    end

    @spec up(t) :: :top | t
    def up(%__MODULE__{up: :top}), do: :top
    def up(%__MODULE__{up: up}), do: up
  end

  @spec stream_decode(ReadCapability.t(), BlockStorage.t()) :: Enumerable.t()
  def stream_decode(%ReadCapability{} = read_capability, block_storage) do
    Stream.resource(
      fn -> Node.from_read_capability(read_capability) end,
      fn node ->
        case traverse_tree(node, block_storage, read_capability.block_size) do
          {_, %Node{}} = acc ->
            acc

          # Halt the stream.
          :halt ->
            {:halt, :halt}

          # If traverse_tree does not return a blocks-node tuple return value as
          # error and halt in next iteration.
          error ->
            {[error], :halt}
        end
      end,
      fn _ -> :ok end
    )
  end

  defp get_block(block_storage, reference) do
    with {:ok, block} <- BlockStorage.get(block_storage, reference) do
      if Crypto.blake2b(block) == reference do
        {:ok, block}
      else
        {:error, :corrupt_block}
      end
    end
  end

  # Traverse the tree and emit block. This implements a depth-first search using
  # the Zipper nodes.

  # end of tree
  defp traverse_tree(%Node{level: 1, right: [], last?: true} = node, _, _), do: {:halt, node}

  # last block requires unpadding
  defp traverse_tree(
         %Node{level: 1, right: [{reference, key} | []], last?: true} = node,
         block_storage,
         block_size
       ) do
    with {:ok, block} <- get_block(block_storage, reference),
         decrypted <- Crypto.chacha20(block, key: key),
         {:ok, unpadded} <- Crypto.unpad(decrypted, block_size: block_size) do
      {[unpadded], Node.right(node)}
    end
  end

  # get and emit block when at level 1
  defp traverse_tree(
         %Node{level: 1, right: [{reference, key} | _]} = node,
         block_storage,
         _block_size
       ) do
    with {:ok, block} <- get_block(block_storage, reference) do
      {[Crypto.chacha20(block, key: key)], Node.right(node)}
    end
  end

  # reached end of a node. Go up, one right and continue there.
  defp traverse_tree(%Node{right: []} = node, _, _) do
    {[], node |> Node.up() |> Node.right()}
  end

  # In the middle of a non leaf node, go down.
  defp traverse_tree(%Node{} = node, block_storage, _) do
    {[], Node.down(node, block_storage)}
  end

  # handle error (non Node) by passing-trough
  defp traverse_tree(error, _, _), do: error
end
