# SPDX-FileCopyrightText: 2020-2021 pukkamustard <pukkamustard@posteo.net>
# SPDX-FileCopyrightText: 2020-2021 rustra <rustra@disroot.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

defmodule ERIS.Encode do
  @moduledoc """
  Stream encoding interface.

  These are low-level interfaces to the ERIS encoding that allow streams of data
  to be encoded.
  """

  alias ERIS.{Crypto, ReadCapability}

  @doc """
  Takes a stream of data block that are correctly chunked to `block_size` and
  encodes them using ERIS using specified block size and convergence secret.

  Returns a stream that emits encrypted blocks (of size `block_size`) and a
  single `ERIS.ReadCapability`.

  This is a low-level interface to the stream encoder and can be used to encode
  content being read from some I/O device. For encoding in-memory data use `ERIS.encode`.
  """
  @spec stream_encode(Enumerable.t(), keyword) :: Enumerable.t()
  def stream_encode(stream, block_size: block_size, convergence_secret: convergence_secret) do
    arity = div(block_size, 64)

    stream
    |> stream_zip_with_last_bool
    |> stream_pad_last(block_size: block_size)
    |> Stream.transform(%{}, fn {data_block, last?}, levels ->
      with {block, reference, key} <-
             encrypt_block(data_block, convergence_secret: convergence_secret),
           levels <- add_to_level(levels, 0, {reference, key}),
           {collected_blocks, levels} <-
             collect(levels, 0, arity: arity, convergence_secret: convergence_secret) do
        if last? do
          {[block] ++
             collected_blocks ++
             finalize(levels, 0, arity: arity, convergence_secret: convergence_secret), levels}
        else
          {[block], levels}
        end
      end
    end)
  end

  # This transforms the stream into a stream of tuples of the original elements
  # and a boolean indicating if element is last of stream
  defp stream_zip_with_last_bool(stream) do
    Stream.zip(
      stream,
      stream |> Stream.drop(1) |> Stream.concat([nil]) |> Stream.map(&is_nil/1)
    )
  end

  defp stream_pad_last(stream, block_size: block_size) do
    Stream.flat_map(stream, fn {block, last?} ->
      if last? do
        with padded <- Crypto.pad(block, block_size: block_size) do
          if byte_size(padded) > block_size do
            [
              {binary_part(padded, 0, block_size), false},
              {binary_part(padded, block_size, block_size), true}
            ]
          else
            [{padded, true}]
          end
        end
      else
        [{block, last?}]
      end
    end)
  end

  defp encrypt_block(unencrypted, convergence_secret: convergence_secret) do
    with key <- Crypto.blake2b(unencrypted, key: convergence_secret),
         block <- Crypto.chacha20(unencrypted, key: key),
         reference <- Crypto.blake2b(block) do
      {block, reference, key}
    end
  end

  defp to_node(level, arity: arity) do
    level
    # concatenate reference-key pairs in level
    |> Enum.map(fn {reference, key} -> reference <> key end)
    # pad with null references
    |> Stream.concat(Stream.cycle([<<0::512>>]))
    # take arity reference-key pairs
    |> Stream.take(arity)
    # and concatenate
    |> Enum.reduce(&(&2 <> &1))
  end

  defp force_collect(levels, level_number, arity: arity, convergence_secret: convergence_secret) do
    with node <- to_node(levels[level_number], arity: arity),
         {block, reference, key} <- encrypt_block(node, convergence_secret: convergence_secret),
         levels <- add_to_level(levels, level_number + 1, {reference, key}),
         levels <- Map.delete(levels, level_number) do
      {[block], levels}
    end
  end

  defp collect(levels, level_number, arity: arity, convergence_secret: convergence_secret) do
    if length(levels[level_number]) >= arity do
      with {blocks, levels} <-
             force_collect(levels, level_number,
               arity: arity,
               convergence_secret: convergence_secret
             ),
           {blocks_from_above, levels} <-
             collect(levels, level_number + 1,
               arity: arity,
               convergence_secret: convergence_secret
             ) do
        {blocks ++ blocks_from_above, levels}
      end
    else
      {[], levels}
    end
  end

  defp finalize(levels, level_number, arity: arity, convergence_secret: convergence_secret) do
    cond do
      level_number === top_level(levels) && length(levels[top_level(levels)]) === 1 ->
        with [{reference, key}] <- levels[level_number] do
          [
            %ReadCapability{
              block_size: arity * 64,
              level: level_number,
              reference: reference,
              key: key
            }
          ]
        end

      length(Map.get(levels, level_number, [])) > 0 ->
        with {blocks, levels} <-
               force_collect(levels, level_number,
                 arity: arity,
                 convergence_secret: convergence_secret
               ),
             blocks_from_above <-
               finalize(levels, level_number + 1,
                 arity: arity,
                 convergence_secret: convergence_secret
               ) do
          blocks ++ blocks_from_above
        end

      Enum.empty?(Map.get(levels, level_number, [])) ->
        finalize(levels, level_number + 1, arity: arity, convergence_secret: convergence_secret)
    end
  end

  defp top_level(levels) do
    levels
    |> Map.keys()
    |> Enum.max()
  end

  defp add_to_level(levels, level_number, {reference, key}) do
    Map.update(levels, level_number, [{reference, key}], &(&1 ++ [{reference, key}]))
  end
end
