# SPDX-FileCopyrightText: 2020-2021 pukkamustard <pukkamustard@posteo.net>
# SPDX-FileCopyrightText: 2020-2021 rustra <rustra@disroot.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

defmodule ERIS do
  @moduledoc """
  Encoding for Robust Immutable Storage (ERIS) is an encoding of content into
  uniformly sized encrypted blocks.

  See http://purl.org/eris
  """

  alias ERIS.{Decode, Encode, ReadCapability}

  defprotocol BlockStorage do
    @doc "Store some data and return the hash reference"
    def put(bs, data)

    @doc "Retrieve from block storage"
    def get(bs, ref)
  end

  defimpl BlockStorage, for: Map do
    def put(map, data) do
      ref = ERIS.Crypto.blake2b(data)

      {:ok, Map.put(map, ref, data)}
    end

    def get(map, ref) do
      case Map.get(map, ref) do
        nil ->
          {:error, :not_found}

        data ->
          {:ok, data}
      end
    end
  end

  defmodule BlockStorage.Dummy do
    @moduledoc """
    A dummy block storage that doesn't store anything. Useful for computing the
    ERIS capability.
    """

    defstruct []

    @type t :: %__MODULE__{}

    def new, do: %__MODULE__{}

    defimpl BlockStorage, for: __MODULE__ do
      def put(dummy, _data), do: {:ok, dummy}

      def get(_dummy, _ref), do: {:error, :not_found}
    end
  end

  @doc """
  Encodes `data` using `block_size` and `convergence_secret` and store blocks in
  `block_storage`.

  Returns a tuple of the read capability and the block storage.
  """
  @spec encode(String.t(), BlockStorage.t(), keyword) :: {ReadCapability.t(), BlockStorage.t()}
  def encode(data, block_storage, opts \\ []) do
    block_size = Keyword.get(opts, :block_size, 1024)
    convergence_secret = Keyword.get(opts, :convergence_secret, <<0::256>>)

    data
    |> chunk_binary(block_size)
    |> Encode.stream_encode(block_size: block_size, convergence_secret: convergence_secret)
    |> Enum.reduce(
      {nil, block_storage},
      fn block_or_root, {root, block_storage} ->
        case block_or_root do
          %ReadCapability{} ->
            {block_or_root, block_storage}

          _ ->
            with {:ok, block_storage} <- BlockStorage.put(block_storage, block_or_root),
                 do: {root, block_storage}
        end
      end
    )
  end

  @doc """
  Encodes `data` and forgets the blocks. This is useful when verifying that
  content has been correctly encoded.

  Returns the read capability as URN string.
  """
  @spec encode_read_capability(String.t(), keyword) :: ReadCapability.t()
  def encode_read_capability(data, opts \\ []) do
    data
    |> encode(BlockStorage.Dummy.new(), opts)
    |> elem(0)
  end

  @doc """
  Encodes `data` and forgets the blocks. This is useful when verifying that
  content has been correctly encoded.

  Returns the read capability as URN string.
  """
  @spec encode_urn(String.t(), keyword) :: String.t()
  def encode_urn(data, opts \\ []) do
    data
    |> encode_read_capability(opts)
    |> ReadCapability.to_string()
  end

  @doc """
  Decodes the content given `read_capability` and `block_storage`. Returns
  encoded content as binary.

  For access to the streaming interface use `ERIS.Decode.stream_decode`.
  """
  @spec decode(ReadCapability.t(), BlockStorage.t()) :: {:ok, binary} | {:error, atom}
  def decode(%ReadCapability{} = read_capability, block_storage) do
    read_capability
    |> Decode.stream_decode(block_storage)
    |> Enum.reduce({:ok, <<>>}, fn block, {:ok, acc} ->
      if is_binary(block), do: {:ok, acc <> block}, else: block
    end)
  end

  def decode(read_capability, block_storage) do
    with {:ok, read_capability} <- ReadCapability.parse(read_capability) do
      decode(read_capability, block_storage)
    end
  end

  @spec chunk_binary(String.t(), number) :: Enumerable.t()
  defp chunk_binary(data, block_size) do
    Stream.resource(
      fn -> data end,
      fn data ->
        case data do
          <<>> ->
            {:halt, <<>>}

          <<head::binary-size(block_size), rest::binary>> ->
            {[head], rest}

          _ ->
            {[data], <<>>}
        end
      end,
      fn _ -> :ok end
    )
  end
end
