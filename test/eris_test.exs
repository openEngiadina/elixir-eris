# SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

defmodule ERISTest do
  use ExUnit.Case
  doctest ERIS

  defmodule TestVector do
    defstruct [
      :id,
      :name,
      :description,
      :content,
      :convergence_secret,
      :block_size,
      :urn,
      :blocks
    ]

    def get_all do
      File.ls!("test/test-vectors/")
      |> Enum.filter(fn file_name -> String.ends_with?(file_name, ".json") end)
      |> Stream.chunk_every(1)
      |> Stream.map(fn [file_name] ->
        with {:ok, json} <- File.read("test/test-vectors/" <> file_name),
             {:ok, test_vector} <- Jason.decode(json) do
          %__MODULE__{
            id: test_vector["id"],
            name: test_vector["name"],
            description: test_vector["description"],
            content: test_vector["content"] |> Base.decode32!(padding: false),
            convergence_secret:
              test_vector["convergence-secret"] |> Base.decode32!(padding: false),
            block_size: test_vector["block-size"],
            urn: test_vector["urn"],
            blocks:
              test_vector["blocks"]
              |> Enum.to_list()
              |> Map.new(fn {reference, block} ->
                {reference |> Base.decode32!(padding: false),
                 block |> Base.decode32!(padding: false)}
              end)
          }
        end
      end)
    end
  end

  test "test vectors" do
    TestVector.get_all()
    |> Stream.each(fn test_vector ->
      with urn <-
             ERIS.encode_urn(test_vector.content,
               block_size: test_vector.block_size,
               convergence_secret: test_vector.convergence_secret
             ) do
        assert urn == test_vector.urn
        assert ERIS.decode(urn, test_vector.blocks) == {:ok, test_vector.content}
      end
    end)
    |> Stream.run()
  end

  test "gracefully handle BlockStorage error" do
    with read_capability <- ERIS.encode_read_capability("Hello!") do
      assert {:error, :not_found} = ERIS.decode(read_capability, %{})
    end
  end

  test "gracefully handle wrong encryption key" do
    with {read_capability, blocks} <- ERIS.encode("Hello!", %{}),
         read_capability <- %{read_capability | key: <<0::256>>} do
      assert {:error, :invalid_padding} = ERIS.decode(read_capability, blocks)
    end
  end

  test "detect corruption" do
    with {read_capability, blocks} <- ERIS.encode("Hello!", %{}),
         corrupted_blocks <-
           Map.replace(blocks, Map.keys(blocks) |> List.first(), <<0::8192>>) do
      assert {:error, :corrupt_block} = ERIS.decode(read_capability, corrupted_blocks)
    end
  end
end
