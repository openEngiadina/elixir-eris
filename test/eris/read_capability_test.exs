defmodule ERIS.ReadCapabilityTest do
  use ExUnit.Case

  alias ERIS.ReadCapability

  doctest ERIS.ReadCapability

  setup do
    {:ok,
     %{read_capability: ERIS.encode_read_capability("Hi, I'm testing stuff. Don't mind me.")}}
  end

  describe "parse/1" do
    test "can parse valid string urn", %{read_capability: read_capability} do
      assert {:ok, %ReadCapability{}} =
               read_capability
               |> ReadCapability.to_string()
               |> ReadCapability.parse()
    end

    test "can parse valid binary read capability", %{read_capability: read_capability} do
      assert {:ok, %ReadCapability{}} =
               read_capability
               |> ReadCapability.to_binary()
               |> ReadCapability.parse()
    end

    test "returns error on invalid binary read capability" do
      assert {:error, :invalid_read_capability} =
               <<1, 2, 3, 4, 5>>
               |> ReadCapability.parse()
    end

    test "returns error on invalid string read capability" do
      assert {:error, :invalid_read_capability} =
               "I surely am not a valid URN"
               |> ReadCapability.parse()
    end

    test "returns error on invalid Base32 encoding" do
      assert {:error, :invalid_read_capability} =
               "urn:erisx2:blups"
               |> ReadCapability.parse()
    end
  end
end
